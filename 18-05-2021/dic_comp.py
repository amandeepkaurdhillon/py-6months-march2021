names = ["Peter", "James", "Harry", "Allen", "Jerry"]
num = [100, 456,432,134,234]

# d = {i:i**3 for i in range(1,10)}
# print(d)

# for i,j in zip(names, num):
#     print(i,j)

marks = {name:mark for name,mark in zip(names,num)}
print(marks)