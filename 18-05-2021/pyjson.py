import json 

student = {
    'name':'Aman',
    'r_no':12,
    'subjects':('HTML','CSS'),
    'is_present':True,
    'is_fail':False,
    'supply':None,
}
print(type(student))
print(student)

to_json = json.dumps(student, indent=4)
print(type(to_json))
print(to_json)

to_py = json.loads(to_json)
print(type(to_py))
print(to_py)