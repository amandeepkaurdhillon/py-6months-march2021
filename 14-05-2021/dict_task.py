st = '''
    Press 1: To add user
    Press 2: To view users
    Press 3: To remove user
    Press 0: To exit
'''
users = []
while True:
    ch = input("Enter Choice: ")
    if ch=="1":
        name = input("Enter Your Name: ")
        email = input("Enter Email Address: ") 
        found=False
        for i in users:
            if i["email"]==email:
                found=True
                print("A user with this email already exist!\n")
        if found==False:
            d = {
                "name":name,"email":email
            }
            users.append(d)
            print("{} added successfully!\n".format(name))
    
    elif ch=="2":
        print("************** Total Users: {} **************".format(len(users)))
        c=1
        for i in users:
            print("{}. Name: {}, Email: {}".format(c,i["name"], i["email"]))
            c+=1
        print()
    elif ch=="3":
        email_to_remove = input("Enter user's email: ")
        flag = False
        for i in users:
            if i["email"]==email_to_remove:
                flag = True
                print("{} removed successfully!\n".format(i["name"]))
                users.remove(i)        
        if flag==False:
            print("User with '{}' email doesn't exist\n".format(email_to_remove)) 

    elif ch=="0":
        break 
    else:
        print("Invalid Choice\n")
