import threading 
import time 
import random

def myfunc1(name, p):
    for i in range(5):
        time.sleep(1)
        print("Process 1: i={} name={}  pin={}".format(i,name,p))

def myfunc2():
    for i in range(5):
        time.sleep(1)
        print("Process 2: i={}".format(i))

# myfunc1()
# myfunc2()

pin = random.randint(1000,9999)
t1 = threading.Thread(target=myfunc1,args=("Aman",pin))
t2 = threading.Thread(target=myfunc2)

t1.start()
t2.start()

t1.join()
t2.join()

t1.setName("Sign Up Process")
print(t1.getName())
print(t2.getName())

print("IT SHOULD EXECUTE AFTER BOTH PROCESSES FINISHED!")