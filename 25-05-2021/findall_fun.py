import re
x = 'At hello 8787 8786 8776767676 there baT cat rat mat chat 987 sat'

# result = re.findall('[a-z]at', x)
# result = re.findall('[a-z]*at', x, re.IGNORECASE)
# result = re.findall('[a-z]*at', x, re.IGNORECASE)
result = re.findall('[0-9]{10}', x)
print(result) 

name = input("Enter Name: ")
check = re.search('[^A-Za-z\s]+',name)
if check !=None:
    print("Name must contain letters only!")
else:
    print("validated!")