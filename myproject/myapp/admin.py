from django.contrib import admin
from myapp.models import Student

admin.site.site_header = "My Project | Admin"

class StudentModel(admin.ModelAdmin):
    # fields = ['name','email']
    list_filter = ['name','registeration_time']
    list_display = ['roll_no','id','name','email','registeration_time','updated_on']
    list_editable = ['name']
    search_fields = ['name','roll_no']
    
admin.site.register(Student, StudentModel)