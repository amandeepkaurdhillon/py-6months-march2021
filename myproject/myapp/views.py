from django.shortcuts import render
from django.http import HttpResponse
import requests
# Create your views here.

def first(request):
    return HttpResponse("First Django View!")

def index(request):
    data = requests.get('https://restcountries.eu/rest/v2/all').json()
    d = {'x':'hello there', 'colors':['red','green','blue','magenta','pink'],
    'data':data}
    return render(request, 'index.html',context=d)

def api(request):
    return render(request,'api.html')

def about(request):
    return render(request,'about.html')