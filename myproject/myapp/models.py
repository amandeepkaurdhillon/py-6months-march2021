from django.db import models

# Create your models here.

class Student(models.Model):
    gender = (
        ('M','Male'),
        ('F','Female'),
    )
    name = models.CharField(max_length=250)
    email = models.EmailField(unique=True)
    roll_no = models.IntegerField(unique=True)
    website = models.URLField(blank=True)
    gender = models.CharField(choices=gender,max_length=250)
    is_register = models.BooleanField(default=False)
    date_of_birth = models.DateField()
    registeration_time = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name+" | "+self.gender
    
    class Meta:
        verbose_name_plural = "Student Table"