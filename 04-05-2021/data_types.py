# Mutable - Changable
# Immutable - could not be changed

a = 10
b = 10.5
c = 'hello 10'
d = True
e = 5+4j
f = [10,20,30] # Mutable
g = (10,20,30) #Immutable
h = {'name':'aman','rno':1}
i = {10,20,20,30,20}
j = None

print(type(a))
print(type(b))  
print(type(c))  
print(type(d))  
print(type(e))  
print(type(f))  
print(type(g))  
print(type(h))  
print(type(i))  
print(type(j))  
print(i)