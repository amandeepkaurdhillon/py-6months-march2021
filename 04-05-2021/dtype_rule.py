a = 'aman'
b = 10
c = '10'

print('hello world')
# print(a+b) = aman10 #(not in python)

# print(a+b) #can only concatenate str (not "int") to str
print(a+c)

## Type casting - change one type to another

print(b+int(c))
print(str(b)+c)

### Python is an interpreter based language  (code executes line by line)

print(a+b)