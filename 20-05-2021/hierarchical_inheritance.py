class Student:
    clz = "ABCD College of Engg. and Technology"
    def __init__(self,name,age,branch):
        self.name = name
        self.age = age 
        self.branch=branch 

class Account(Student):
    def fee_details(self,total,pending):
        print("Here are fee details of: {}".format(self.name))
        print("Total: Rs.{}/- Pending:Rs.{}/-".format(total,pending))

class Introduction(Student):
    def get_intro(self):
        print("Name: {}".format(self.name))
        print("Age: {}".format(self.age))
        print("Branch: {}".format(self.branch))

s1 = Account("Aman","22","CSE")
s1.fee_details(10000,9000)
s2 = Introduction("Peter","22","ME")
s2.get_intro()

s2.clz="XYZ College"
print(s2.clz)

print(s1.clz)
