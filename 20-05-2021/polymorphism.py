# a = 1
# b = 2

# print(a+b) #operator overloading

class MyClass:
    def add(self, x,y):
        print(x+y)

    def add(self,x,y,z):
        print(x+y+z)

    
obj = MyClass()
obj.add(10,30,5)
# obj.add(10,30) ##Python doesn't support function overloading