class Parent:
    a = "Hello There"
    def intro(self):
        print("Parent member function")

class Child(Parent):
    a = "Hii All"
    def intro(self):
        print("a =",super().a)
        # print("a =",self.a)
        print("Child class function")

ob = Child()
# print(ob.a)
ob.intro()