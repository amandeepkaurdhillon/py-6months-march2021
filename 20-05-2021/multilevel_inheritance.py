class GrandParent: # a, intro1
    a = "hello there"
    def intro1(self):
        print("MF of Grand parent class")

class Parent(GrandParent): # b,intro2, a, intro1
    b = "How are you?"
    def intro2(self):
        print("Member Function of Parent class")

class Child(Parent): #c, intro3, b, intro2, a, intro1
    c = "What are you doing?"
    def intro3(self):
        print("MF of child class")

obj1 = Parent()
obj = Child()
print(dir(obj))
print(obj.a, obj.b, obj.c)
obj.intro1()
obj.intro2()
obj.intro3()

print(issubclass(Child, Parent))