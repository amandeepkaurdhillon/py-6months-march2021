import random
class Account:
    bank = "Axis Bank" #Public data member
    __ifsc = "AXIS0001" #Private data member
    __acc_no = random.randint(100000,999999)
    def create_account(self, name): #Public member function
        print("{} Account created successfully".format(name))
        print("IFSC CODE: ", self.__ifsc)
        print("ACC Number: ", self.__acc_no)
    def __abcd(self):
        print("It is accessible only inside the class")

cus1 = Account()
print(cus1.bank)
cus1.create_account("Peter")
# print(cus1.__ifsc) ## Object can't access private data members
# print(dir(cus1))
# print(cus1.__abcd()) ## Object can't access private member functions