class  Library:
    location='Block A'
    def issue_book(self, bkname, isdt, rtdt):
        print("Book Name:",bkname)
        print("Issued On:",isdt)
        print("Due Date:",rtdt)

class Account:
    clz = 'XYZ College of Management'
    def fee_details(self,total,paid):
        print("Total: Rs.{}/-\nPending:Rs.{}/-".format(total, total-paid))

#Multiple Inheritance
class Student(Library,Account):
    def register(self,name,rno):
        print("Registered Successfully in {}!".format(self.clz))
        print("Name:{} Roll No.: {}".format(name, rno))
        super().issue_book("Python Programming", "13-05-21","28-05-21")

obj = Student()
obj.register("James",1001)
# print(obj.clz)
obj.fee_details(170000,100000)
print()
obj1 = Student()
obj.register("Peter",1002)
obj.fee_details(170000,170000)

