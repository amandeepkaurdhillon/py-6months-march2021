try:
    name = input("Enter Your Name: ")
    r_no = input("Enter Your roll number: ")
    filepath = 'users/{}.txt'.format(r_no)
    file = open(filepath,'x')
    txt = 'Name: {}\nRoll Number: {}'.format(name, r_no)
    file.write(txt)
    file.close()
    print("Dear {} you registered successfully!".format(name))

except FileExistsError:
    print("A user with this roll number already exists!")