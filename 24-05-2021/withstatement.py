# f = open('dummy.txt')
# print("before: ", f.read())
# f.close()

with open('dummy.txt', 'r') as f:
    print('before: ', f.read())

# print('After: ', f.read()) ## File will close automatically