import requests

try:
    city = input("Enter City Name: ")
    API_URL = "http://api.openweathermap.org/data/2.5/weather?q={}&appid=24e304c25b4fe529f93ace7951e9e468&units=metric".format(city)
    data = requests.get(API_URL).json()

    weather = data['weather'][0]['main']
    des = data['weather'][0]['description']
    temp = data['main']['temp']
    icon = data['weather'][0]['icon']

    html = '''
        <div style="width:300px;margin:100px auto;padding:20px;box-shadow:0px 0px 10px gray;text-align:center;background:black;color:white;">
            <img src='http://openweathermap.org/img/wn/{}@2x.png' >
            <h1>{}</h1>
            <p><em>({})</em></p>
            <p>Temp: {}&deg;C</p>
        </div>
    '''.format(icon, weather, des, temp)

    filename = '{}.html'.format(city)
    file = open(filename,'w')
    file.write(html)
    file.close()
    print('{} written successfully!'.format(filename))

except:
    print("Could not get weather! Something went wrong!")