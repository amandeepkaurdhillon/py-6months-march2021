col = ["red", "green", "blue","orange","black","pink"]
a = ["hello", "there", 10, 0.10, True]

#Indexing 
print(col[0])
print(col[-1])

# Slicing
print(col[:2])
print(col[-3:])

#steping
print(col[::2])
print(col[::3])

print(col[::-2])
print(col[::-1])

# Concatenation
print(col+a)

#Membership 
print("red" in col)
print("red" in a)
print("red" not in a)

#Identity 
x = "hello"
y = "hello"
print(x is y)
print(x is not y)

#Iteration 
print(a*3)