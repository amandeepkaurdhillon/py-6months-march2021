# print(dir([]))
a = [10,3,42,2,1,0,87]
a.sort()
a.reverse()
print(a)

games = ["Candy Crush","Temple Run" ,"Clash of Clanes", "Snake", "Temple Run"]
print(games)
games.append("PubG")
games.insert(1, "Pokemon")

c_games = games.copy()
games.clear()

print(c_games)
c_games.pop()
c_games.remove("Snake")
print(c_games)

print(c_games.index("Temple Run"))
print(c_games.count("Temple Run"))

a = [10,20]
b = [30,40]

# a.extend(b)
# # a.append(b)
# print(a)
a[1]="Python"
# del a ## To delete complete list
print(a)

