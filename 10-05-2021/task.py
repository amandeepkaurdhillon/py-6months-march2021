num = input("Enter Mobile Number: ")
if num.isdigit()==True:
    if len(num)>=10:
        n = len(num[:-3])
        print("Here is your mobile number: ", n*"*"+num[-3:])
    else:
        print("Mobile number must contain atleast 10 digits!")
else:
    print("Number must contain digits only!")