def table(x):
    """
    Function to return table of given number
    Example: table(2)
    Will return table of 2
    """
    for i in range(1,11):
        print("{} x {} = {}".format(x,i,x*i))
    print("=====================================================\n")

table(10)
# table(17)
# table(7)
print(table.__doc__)

# Docstrings of built in functions
print(len.__doc__)
print(print.__doc__)
