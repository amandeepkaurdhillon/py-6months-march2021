def profile(name,age,gender):
    print("Name: {}\nAge: {}\nGender: {}\n".format(name,age,gender))


# Positional arguments
profile("Peter",19,"M")
profile(19,"M","James")

#Keyword Arguments
profile(age=19,gender="M",name="James")
profile("Jenny",22,gender="F")


