a = set()

a.add(10)
a.add(10)
a.add(4)
a.add(4)
a.add(5)

# print(a)
# print(dir(a))

b = {10,20,30,40}
c = {20,40,70}
#Union
print(b|c)
print(b.union(c))

#intersection
print(b&c)
print(b.intersection(c))

#difference
print(b-c)
print(c.difference(b))

print(b.symmetric_difference(c))


