x = {10,20,30,40}
y = {10,40,77}

print(y.issubset(x))
print(x.issubset(y))
print(x.issuperset(y))

if x.issuperset(y):
    print("x is superset of y")
else:
    print("x is not superset of y")
