def Area(r=0, pi=3.14):
    return "pi = {} r = {} Area = {}".format(pi,r,pi*r**2)

# print(Area(10,3.14))
# print(Area(5))
# print(Area())
# print(Area(2,4))

r = int(input("Enter Radius: "))
print(Area(r))