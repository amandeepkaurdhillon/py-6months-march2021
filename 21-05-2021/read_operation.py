# f = open('myfile.txt', 'rb')
f = open('myfile.txt', 'r')

# data = f.read()
# data = f.read(10)

# data = f.readline()
# print(data)
# print(f.readline())
# print(f.readline())

data = f.readlines()
# print(data[:-2])
print("Total lines: ", len(data))
for i in data[-2:]:
    print(i)

print(type(data[0]))

f.close()