try:
    emp = {'name':'Eric', 'emp_code':'A10021'} 
    print(emp)
    import abcd
except:
    print('Error in try! Except Block') 

else:
    print('No error in try block! Else Part') 

finally:
    print('I will always execute! Finally Block ')