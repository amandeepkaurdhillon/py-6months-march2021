try:
    a,b=10,20
    print(a+b)
    c = [10,20,30]
    d = {'name':'peter'}
    print(d['email'])
    # print(c[4])
    # print(a/0)
except (IndexError, KeyError):
    print("Please check keys & indexes properly!")
except NameError:
    print("PLease Check variable names carefully!")
except ZeroDivisionError:
    print("Number can't be divided by zero")

