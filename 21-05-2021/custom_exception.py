class MyError(Exception):
    def __init__(self, v='First number must less than second'):
        self.v = v 

    def __str__(self):
        return self.v
try:
    n1 = int(input("Enter First Number: "))    
    n2 = int(input("Enter Second Number: "))
    if n1>n2:
        raise MyError
    for i in range(n1, n2+1):
        print(i)

except MyError as value:
    print(value)
    print("Something went wrong!")