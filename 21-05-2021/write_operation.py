f = open('abcd.txt', 'w')
print("Pointer: ", f.tell())

f.write("I am written with python")
f.write("How are you?\n")

for i in range(1,11):
    f.write('{}x{}={}\n'.format(3,i,3*i))

print("Pointer: ", f.tell())
f.close()