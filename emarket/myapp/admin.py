from myapp.views import checkout
from django.contrib import admin
from myapp.models import (Contact_Us, Brand, Customer_Category, Product_Category,Product, 
Product_Images, UserProfile, Add_To_Favourite,cart,checkout,Order)
# Register your models here.

admin.site.site_header = ("My project | Admin") 

class Contact_Us_Model(admin.ModelAdmin):
     # fields = ['name','email']
    list_filter = ['name','subject']
    list_display = ['id','name','message','received_on','is_approved']
    list_editable = ['is_approved']
    search_fields = ['name','subject']

class BrandModel(admin.ModelAdmin):
    list_filter = ['name','added_on']
    list_display =['id','name','added_on','updated_on']
    search_fields = ['name']

class Cust_Cat(admin.ModelAdmin):
    list_filter = ['name','added_on']
    list_display =['id','name','added_on','updated_on']
    search_fields = ['name']

class Prod_Cat(admin.ModelAdmin):
    list_filter = ['category_name','added_on','customer_category']
    list_display = ['id','category_name','added_on','updated_on','customer_category']
    search_fields = ['category_name','customer_category']

class Products(admin.ModelAdmin):
    list_filter = ['name','brand','is_available',]
    list_display = ['id','name','price','added_on','updated_on','is_available']

class Prd_img(admin.ModelAdmin):
    list_display = ['id','product',]
    
class check(admin.ModelAdmin):
    list_display=['id','first_name']

admin.site.register(Contact_Us,Contact_Us_Model)
admin.site.register(Brand, BrandModel)
admin.site.register(Customer_Category,Cust_Cat)
admin.site.register(Product_Category,Prod_Cat)
admin.site.register(Product,Products)
admin.site.register(Product_Images,Prd_img)
admin.site.register(UserProfile)
admin.site.register(Add_To_Favourite)
admin.site.register(cart)
admin.site.register(checkout,check)
admin.site.register(Order)