import json
from os import stat_result, truncate
from django import http
from django.core.checks import messages
from django.db.models.expressions import F
from django.db.models.fields import BLANK_CHOICE_DASH, EmailField
from django.http.response import HttpResponseBase, HttpResponseGone, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, get_object_or_404,reverse
from myapp.models import (Contact_Us,Brand,Customer_Category, Product,Product_Category,
Product_Images, UserProfile, Add_To_Favourite,cart,checkout,Order)
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout
from django.db.models import Q, query
from paypal.standard.forms import PayPalPaymentsForm
from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMessage
from django.conf import settings
import  random

# Create your views here.
def home(request):
    context = {}
    context['all_brands'] = Brand.objects.all()
    context['main_category'] = Customer_Category.objects.all()
    context['product_category'] = Product_Category.objects.all()
    final = []
    no_carousel_item= 2
    start,end = 0, 4
    for i in range(no_carousel_item):
        recent_products=[]
        for product in Product.objects.all().order_by('-id')[start:end]:
            p={ "id":product.id,
                "name":product.name, "brand":product.brand.name, "price":product.price,
                "discounted_price":product.price-product.discount,
                "dis_per":round((product.discount/product.price)*100)
            }
            images = Product_Images.objects.filter(product__id=product.id)
            imgs = [i.product_image for i in images]    
            p["images"] = imgs  
            recent_products.append(p)
        start=end 
        end = end*2
        final.append(recent_products)

    context["recent_products"] = final
    return render(request,'E-commerce-Home.html', context)

def product_detail(request):
    context={}
    id = request.GET.get('id')
    all = []
    for product in Product.objects.filter(id=id):
        p={ "id":product.id,
                "name":product.name, "brand":product.brand.name, "price":product.price,
                "discounted_price":product.price-product.discount,"size":product.size,
                "color":product.color,"description":product.description,
                "dis_per":round((product.discount/product.price)*100)
            }
        images = Product_Images.objects.filter(product__id=product.id).all()
        imgs = [i.product_image for i in images]    
        p["images"] = imgs  

    all.append(p)
    context['product']= all
    
    return render(request,'E-commerce-product-detail.html',context)
    
def shop(request):
    context = {}
    context['all_brands'] = Brand.objects.all()
    return render(request,'E-commerce_Shop_page.html',context)



def create_user(request):
    data = {}
    if "sign-in" in request.POST:
        phn = request.POST["phone_num"]
        pwd = request.POST["password"]

        user = authenticate(username=phn,password=pwd)
        if user:
            login(request,user)
            if user.is_superuser or user.is_staff:
                return HttpResponseRedirect("/admin")
            if user.is_active:
                return HttpResponseRedirect("/dashboard")

        else:
            return render(request,"E-commerce-login.html",{"message":"Invalid Username or Password"})

    if "register" in request.POST:
        first_name = request.POST.get("first_name")
        last_name = request.POST["last_name"]
        username = request.POST["phone_number"]
        email = request.POST["email"]
        password = request.POST["password"]

        usr = User.objects.create_user(username, email, password)
        usr.first_name = first_name
        usr.last_name = last_name
        usr.save()
        if usr:
            profile = UserProfile(user=usr, phone_number=username, email_address=email)
            profile.save()
            data["message"] = "{} {} registered successfully!".format(first_name, last_name)

    return render(request,'E-commerce-login.html', data)

def productcart(request):
    context = {}
    items = cart.objects.filter(user__id=request.user.id,status=False)
    context["items"] = items
    if request.user.is_authenticated:
        if request.method=="POST":
           pid = request.POST["pid"]
           qty = request.POST["qty"]
           img = request.POST["img"]
           dis_price = request.POST["dis_price"]
           is_exist = cart.objects.filter(product__id=pid,user__id=request.user.id,status=False)
           if len(is_exist)>0:
               context["msg"] = "item already exist in cart"
               context["cls"] = "alert alert-warning"
           else:
              product = get_object_or_404(Product,id=pid)
              usr = get_object_or_404(User,id=request.user.id)
              c = cart(user=usr,product=product,quantity=qty,image=img,total_price=dis_price)
              c.save()
              context["msg"] = "{} Added in cart".format(product.name)
              context["cls"] = "alert alert-success"
              
    else:
        context["status"] = "Please login first to add products to cart"
    return render(request,'E-commerce-cart.html',context)

def checkoutproduct(request):
    context={}
    check = checkout.objects.filter(is_approved=True)
    context['cart'] = cart.objects.filter(user__id=request.user.id,status=False)
    context['all'] = check

    if request.method=="POST":
        fn = request.POST['first_name']
        ln = request.POST['last_name']
        ctry = request.POST['country_name']
        st = request.POST['state_name']
        cty =request.POST['city_name']
        pc = request.POST['postcode']
        addr = request.POST['address']
        ph = request.POST['phone_no']
        em = request.POST['email_address']
        
        data = checkout(first_name = fn, last_name=ln,country=ctry,state=st,city=cty,
        address=addr,postcode=pc,phone_number=ph,email_address=em)
        data.save()
    return render(request,'E-commerce_Checkout.html',context)


def find_us(request):
    context = {}
    all_obj = Contact_Us.objects.filter(is_approved=True)
    # all_obj = Contact_Us.objects.filter(name="James")
    # all_obj = Contact_Us.objects.all().order_by('-id')
    context['all'] = all_obj

    if request.method=="POST":
        # print(request.POST)
        name = request.POST['name']
        email = request.POST['email']
        sub = request.POST['subject'] 
        msz = request.POST['message']
        
        con = Contact_Us(name=name, email=email, subject=sub, message=msz)
        con.save()

        context['message']="Dear {} Thanks for your time!".format(name)
        
    return render(request,'E-commerce_how-to-find.html', context)

def blog(request):
    return render(request,'E-commerce-blog.html')
def base(request):
    context = {}
    context['main_category'] = Customer_Category.objects.all()
    
    return render(request,'base.html',context)

def all_categories(request):
    all = []
    for cust in Customer_Category.objects.all():
        products=list(Product_Category.objects.filter(customer_category__id=cust.id).values())
        d = {"id":cust.id,"customer_category_name":cust.name, "products":products,}
        all.append(d)
    
    return JsonResponse(all, safe=False)

def brand(request):
    data=[]
    for brand in Brand.objects.all():
        brand_sort=list(Product.objects.filter(brand__id=brand.id).values())
        d= {"id":brand.id,"brand_name":brand.name,"brand_sort":brand_sort}
        data.append(d)
    return JsonResponse(data,safe=False)   

def check_user(request):
    phone_num = request.GET.get("phone_num")
    total_user = User.objects.filter(username=str(phone_num))
    if len(total_user)>0:
        return JsonResponse({"status":1, "message":"A user with this phone number already exists"})
    else:
        return JsonResponse({"status":0, "message":"success"})

def product_filter_api(request):
    if 'cat_id' in request.GET:
        cat_id = request.GET['cat_id']
        products = list(Product.objects.filter(category__id=cat_id).values())
    elif 'brand_id' in request.GET:
        brand_id = request.GET['brand_id']
        products = list(Product.objects.filter(brand__id=brand_id).values())
    else:
        products = list(Product.objects.all().values())


    for product in products:
        images = list(Product_Images.objects.filter(product__id = product['id']).values())
        product["images"] = images
    for brand in products:
        br = Brand.objects.get(product__id = brand['id']).name
        brand["brand"] = br
        
    return JsonResponse(products, safe=False)

def filter_product(request):
    query = request.GET.get("query")
    prds = Product.objects.filter(Q(name__icontains=query)|Q(brand__name__icontains=query))
    all=[]
    for product in prds:
        p = {
            "id":product.id,
            "name":product.name,
            "brand":Brand.objects.get(id=product.brand_id).name,
            "ac_price":product.price,
            "of_price":product.price-product.discount,
            "image":Product_Images.objects.filter(product__id=product.id).values()[0].get('product_image'),
        }
        all.append(p)
        
    return JsonResponse({"products":all})

def add_to_favourite(request):
    id = request.GET.get("id")
    res = {}
    if request.user.is_authenticated==False:
        res.update({'status':0, 'message':"Please login first to add products to favorites"});        
    elif request.user.is_superuser:
        res.update({'status':0, 'message':"Admin is not allowed to create favourite list"});
    else:
        results=Add_To_Favourite.objects.filter(customer__user__id=request.user.id, product__id=id)
        if len(results)==0:
            usr = get_object_or_404(UserProfile, user__id=request.user.id)
            prd = get_object_or_404(Product, pk=id)
            obj = Add_To_Favourite(customer=usr, product=prd)
            obj.save()
            res.update({'status':1,'message':'Added Successfully!','id':id})
        else:
            Add_To_Favourite.objects.get(customer__user__id=request.user.id, product__id=id).delete()
            res.update({'status':2,'message':'Deleted Successfully!','id':id})
    return JsonResponse(res)

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/')

def all_favourites(request):
    results=Add_To_Favourite.objects.filter(customer__user__id=request.user.id)
    product = Product.objects.all()
    ids=[]
    det = []
    for i in results:
        ids.append(i.product.id)
    for c in ids:
        for d in product:
            v = str(d.id)
            n = str(c)
            if(n == v):
               p = {
                   "id":d.id,
                   "name":d.name,
                   "brand":Brand.objects.get(id=d.brand_id).name,
                   "ac_price":d.price,
                   "of_price":d.price-d.discount,
                   "image":Product_Images.objects.filter(product__id=d.id).values()[0].get('product_image'),
                   }
               det.append(p)
    return JsonResponse({'favourites':ids,'products':det}) 

def forgotpass(request):
    context = {}
    if request.method=="POST":
        un = request.POST["username"]
        pwd = request.POST["npass"]
        user = get_object_or_404(User,username=un)
        user.set_password(pwd)
        user.save()
        context['status'] = 'Password Changed Successfully!!!'
    return render(request,'forgot_pass.html',context)

def resetpass(request):
        un = request.GET["username"]
        try:
          user = get_object_or_404(User,username=un)
          otp = random.randint(1000,9999)
          msg = "Dear {}. Here is your OTP {}".format(user.first_name,otp)
          try:
              email = EmailMessage("Account Verification For Password Reset",msg,to=[user.email])
              email.send()
              return JsonResponse({"status":"sent","email address":user.email,"rotp":otp})
          except:
              return JsonResponse({"status":"error"})
        except:
            return JsonResponse({"status":"failed"})

def get_cart_data(request):
    items = cart.objects.filter(user__id=request.user.id,status=False)
    sale,total,quantity=0,0,0
    for i in items:
        sale+=i.product.discount*i.quantity
        total+=i.product.price*i.quantity
        quantity+=i.quantity

    res={
        "total":total,"offer":sale,"quan":quantity
    }
    return JsonResponse(res)
    
def change_quan(request):
    if "quantity" in request.GET:
        qty = request.GET["quantity"]
        cid = request.GET["cid"]
        cart_obj = get_object_or_404(cart,product__id=cid)
        cart_obj.quantity = qty
        cart_obj.save()
        return HttpResponse(cart_obj.quantity)

    if "delete_cart" in request.GET:
        id = request.GET['delete_cart']
        cart_obj = get_object_or_404(cart,product__id=id)
        cart_obj.delete()
        return HttpResponse(1)
        

def dashboard(request):
    context = {}
    check = UserProfile.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = UserProfile.objects.get(user__id=request.user.id)
        context["data"]=data    
    if request.method=="POST":
        fn = request.POST["fname"]
        ln = request.POST["lname"]
        em = request.POST["email"]
        con = request.POST["contact"]

        usr = User.objects.get(id=request.user.id)
        usr.first_name = fn
        usr.last_name = ln
        usr.email = em
        usr.username = con
        usr.save()

        data.email_address=em
        data.save()

        if "image" in request.FILES:
            img = request.FILES["image"]
            data.profile_pic = img
            data.save()
            ch = User.objects.filter(user__id=request.user.id)


    ch = User.objects.filter(id=request.user.id)
    if len(ch)>0:
        data = User.objects.get(id=request.user.id)
        context["data"] = data

    all_orders = []
    orders = Order.objects.filter(cust_id__id=request.user.id).order_by("-id")
    for order in orders:
        products = []
        for id in order.product_ids.split(",")[:-1]:
            pro = get_object_or_404(Product, id=id)
            products.append(pro)           
        ord = {
            "order_id":order.id,
            "products":products,
            "invoice":order.invoice_id,
            "status":order.status,
            "date":order.processed_on,
        }
        print(ord)
        all_orders.append(ord)
    context["order_history"] = all_orders
            
    context["status"] = "Changes Saved Successfully(Refresh Page To See The Changes)"

    return render(request,'dashboard.html',context)


def process_payment(request):
    item = cart.objects.filter(user_id__id = request.user.id,status=False)
    products=""
    amt = ""
    inv="INV10001-"
    cart_ids=""
    p_ids=""
    for s in item:
        products= str(s.product.name)+","
        amt+=float(s.total_price)
        p_ids+=str(s.product.id)+","
        inv+=str(s.id)
        cart_ids+=str(s.id)+","
    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': str(amt),
        'item_name': products,
        'invoice':inv,
        'notify_url': 'http://{}{}'.format('127.0.0.1:8000',
                                           reverse('paypal-ipn')),
        'return_url': 'http://{}{}'.format('127.0.0.1:8000',
                                           reverse('payment_done')),
        'cancel_return': 'http://{}{}'.format('127.0.0.1:8000',
                                              reverse('payment_cancelled')),

    }
    usr = User.objects.get(username=request.user.username)
    ord = Order(cust_id=usr,cart_ids=cart_ids,product_ids=p_ids )
    ord.save()
    ord.invoice_id = str(ord.id)+inv
    ord.save()
    request.session["order_id"] = ord.id
    
    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'process_payment.html', {'form': form})

def payment_done(request):
    if "order_id" in request.session:
        order_id = request.session["order_id"]
        ord_obj = get_object_or_404(Order,id=order_id)
        ord_obj.status=True
        ord_obj.save()
        
        for i in ord_obj.cart_ids.split(",")[:-1]:
            cart_object = cart.objects.get(id=i)
            cart_object.status=True
            cart_object.save()
    return render(request,"payment_success.html")

def payment_cancelled(request):
    return render(request, 'payment_failed.html')
