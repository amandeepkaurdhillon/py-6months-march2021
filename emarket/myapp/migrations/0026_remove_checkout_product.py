# Generated by Django 3.2.3 on 2021-07-03 18:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0025_remove_checkout_cart'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='checkout',
            name='product',
        ),
    ]
