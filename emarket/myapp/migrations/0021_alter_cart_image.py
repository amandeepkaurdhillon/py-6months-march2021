# Generated by Django 3.2.3 on 2021-07-01 07:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0020_alter_cart_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cart',
            name='image',
            field=models.ImageField(default=False, upload_to='cart/%Y/%m/%d'),
        ),
    ]
