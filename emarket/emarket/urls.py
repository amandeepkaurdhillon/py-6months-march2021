"""emarket URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from os import name
from django.contrib import admin
from django.urls import path,include
from myapp import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.home,name='home'),
    path('shop/',views.shop,name='shop'),
    path('login/',views.create_user,name='login'),
    path('logout/',views.user_logout,name='user_logout'),
    path('cart/',views.productcart,name='cart'),
    path('checkout/',views.checkoutproduct,name='checkout'),
    path('product_detail/',views.product_detail,name='prodct-detail'),
    path('find_us/',views.find_us,name='find'),
    path('blog/',views.blog,name='blog'),
    path('base/',views.base),
    path('api/categories',views.all_categories,name="all_categories"),
    path('api/brand',views.brand,name="brand"),
    path('api/products',views.product_filter_api,name="product_filter_api"),
    path('user_check/',views.check_user,name="check_user"),
    path('filter_product/',views.filter_product,name="filter_product"),
    path('add_to_favourite/',views.add_to_favourite,name="add_to_favourite"),
    path('all_favourites/',views.all_favourites,name="all_favourites"),
    path('forgotpass',views.forgotpass,name="forgotpass"),
    path('resetpass',views.resetpass,name="resetpass"),
    path('get_cart_data',views.get_cart_data,name="get_cart_data"),
    path('change_quan',views.change_quan,name='change_quan'),
    path('dashboard',views.dashboard,name='dashboard'),
    path('paypal/', include('paypal.standard.ipn.urls')),
    path('process_payment',views.process_payment,name='process_payment'),
    path('payment_done',views.payment_done,name='payment_done'),
    path('payment_cancelled',views.payment_cancelled,name='payment_cancelled'),
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)

