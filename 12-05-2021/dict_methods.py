emp = {
    "name":"Jay",
    "salary":"$10000",
    "company":"XYZ Pvt Ltd.",
}
# print(dir(emp))

# # Get value 
# print(emp["name"])
# print(emp.get("name1"))

# #Insert 
# emp["gender"]="male"
# #Update
# emp["name"]="Andrina" #It will update name
# emp.update({"city":"New York", "gender":"female"})
# #delete
# print(emp.pop("gender"), "removed!")
# print(emp.popitem(), "removed!")
# del emp["name"]

print(emp)

# print(emp.keys())
# print(emp.values())
# print(emp.items())

# for i in emp.items():
#     print(i)

ls = ["name", "max_marks","obtained_marks"]
d = {}
print(d.fromkeys(ls))
print(d.fromkeys(ls,0))
print(d.fromkeys(ls,"Not Set"))

