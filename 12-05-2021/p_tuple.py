a = 10,20,30
b = ("Pyhon", "PHP", "HTML")

print(type(a))
print(a)
print(b)

print(b[1])
print(b[-1])
print(b[1:])
print(b[::2])

#python tuple is an immutable type
# b[1]="CSS"
# del b[1]
# print(dir(b))

c = (10,10,20,30,40,10)

print(c.count(10))
print(c.index(10))
print(c.index(10,2))
print(c.count(11))