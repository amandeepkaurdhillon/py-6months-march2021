print()
print("Press 1: To add element") 
print("Press 2: To view element")
print("Press 3: To remove element")
print("Press 4: To Sum")
print("Press 5: To multiply")
print("Press 0: To exit")
print()
a = []
while True:
    ch = input("Enter Choice: ")
    if ch=="1":
        n = int(input("Enter Number to add: "))
        a.append(n)
        print("{} added successfully!\n".format(n))

    elif ch=="2":
        print("Total: {}".format(len(a)))
        print("{} \n".format(a))

    elif ch=="3":
        no_to_remove = int(input("Enter Number to Remove: ")) 
        if no_to_remove in a:
            a.remove(no_to_remove)
            print("{} removed successfully!\n".format(no_to_remove))
        else:
            print("{} not found in list\n".format(no_to_remove))

    elif ch=="4":
        sum = 0
        for i in a:
            sum = sum+i
        print("Total Sum: {}\n".format(sum))

    elif ch=="5":
        mul = 1
        for i in a:
            mul *= i 
        print("Multiplication: {}\n".format(mul)) 
    elif ch=="0":
        break   
    else:
        print("Invalid Choice\n")