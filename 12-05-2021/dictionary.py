student = {
    "name":"Amandeep Kaur",
    "r_no":12,
    "is_present":True,
    "subjects":["Computer Graphics", "Networking","DBMS"],
    "technology":{
        "front-end":["HTML","CSS","JS"],
        "backend":["Python","Django","SQLite"],
    }
}

print(student["name"])
print(student["subjects"][1])
print(student["technology"]["backend"][2])