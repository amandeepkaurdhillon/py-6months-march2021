emp = {
    "name":"Jay",
    "salary":"$10000",
    "company":"XYZ Pvt Ltd.",
    "gender":"male",
    "country":"US",
    "email":"jay@gmail.com",
}

# for i in emp:
#     print(i,"=>",emp[i])

for k,v in emp.items():
    print(k, "=>", v)
