a = "Python is an Easy Language"

print(a)
print(a.upper())
print(a.lower())
print(a.title())
print(a.capitalize())
print(a.swapcase())
print()

b = 'hello'
c = 'a987658876865'
d = 'red,green,blue'

print(b.isupper())
print(b.istitle())
print(b.islower())
print(c.isdigit())

print(d.split(',')[0])
print(d.split(',')[1])
print(c.split('8'))

print(c.count('a'))
print(c.count('b'))
print(c.count('8'))
# print(dir(''))
# a[0]="A" ##It will return error as python string is immutanle-not support element edit delete

# print(a)
# del a 
# print(a)