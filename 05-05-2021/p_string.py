a = 'Python is created by Guido van Rossum'
b = 'Hello'
print(a)
# Indexing 
print(a[1])
print(a[3])

# Python supports negative/reverse indexing as well
print(a[-1])

# Check number of elements in a string 
print(len(b))

# Slicing 
print(a[2:])    
print(a[:2])
print(a[-6:])
print(a[:-6])

#Stepping
nm = "My Name is Aman"
nm = "0123456789"
print(nm[::1])
print(nm[::2])
print(nm[::3])
print(nm[::-1])
print(a[::-1])
