st = "Hyper Text"

# st[i]=val
for i in range(len(st)):
    # print("st["+i+"] = "+st[i]) ## It will return error
    print("st[{}] = {}".format(i, st[i]))
    # print(i,st[i])

## Task
# enter number: 8

# 8*1 = 8
# 8*2 = 16
# 8*3 = 24
# ...
# ...
# ...
# 8*10 = 80