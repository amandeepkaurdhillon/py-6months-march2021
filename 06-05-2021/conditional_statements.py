a = 100

## if statement
# if a<20:
#     print("A is lesser")

## if else

if a<20:
    print(a, "is lesser than 20")
else:
    print(a, "is greater than 20")