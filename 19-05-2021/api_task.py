import requests

data = requests.get("https://restcountries.eu/rest/v2/all").json()

country = input("Enter Country Name: ").capitalize()

found = False
for i in data:
    if i["name"] == country:
        found = True
        print(">>Name: {}".format(i["name"]))
        print(">>Population: {}".format(i["population"]))
        print(">>Capital: {}".format(i["capital"]))
        print(">>Region: {}".format(i["region"]))
        print(">>Area: {}".format(i["area"]))
        print(">>Borders:")
        c = 1
        for borders in i["borders"]:
            print("   {} : {}".format(c,borders))
            c+=1
        print(">>Languages:")
        for lang in i["languages"]:
            for c,v in lang.items() :
              print("   {} : {}".format(c,v))
if found == False: 
    print("SORRY !!! Country is not in API")    