class Circle:
    pi=3.14
    def __init__(self,r):
        self.r = r

    def get_area(self):
        print("r={} area={}".format(self.r, self.pi*self.r**2))

c1 = Circle(10)
c2 = Circle(4)
c3 = Circle(160)

c1.get_area()
c3.get_area()
