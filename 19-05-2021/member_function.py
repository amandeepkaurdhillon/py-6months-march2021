class Circle:
    pi = 3.14

    def set_radius(self,r):
        self.radius = r
        print("Radius Set!")
    
    def get_area(self):
        print("pi={} r={} r={}".format(self.pi,self.radius,self.pi*self.radius**2))

c1 = Circle()
c2 = Circle()
c3 = Circle()

c1.set_radius(10)
c2.set_radius(7)
c3.set_radius(0)

c1.get_area()
c2.get_area()
c3.get_area()

    