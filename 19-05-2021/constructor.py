class MyClass:
    def __init__(self):
        self.name = "Amandeep Kaur"
        print("I will call automatically!\n")
        # return self.name  ## cnstructor should return None not string

    def myfun(self):
        print("I'm a member function: ", self.name)

    def __del__(self):
        del self.name
        print("\n I'm a destructor")
        # print(self.name) ##It will throw error

obj = MyClass()
obj.myfun()
