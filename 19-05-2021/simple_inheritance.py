class Library:
    clz = "ABCD Engg. College"
    def book_details(self):
        print("There are 10k books in library")

#Single Inheritance
class Student(Library):
    branch = "ECE"
    def intro(self):
        print("My Name is Amandeep kaur!")

ob = Student()
print(ob.branch)
print(ob.clz)
ob.intro()
ob.book_details()

obj=Library()
print(obj.clz)
# print(obj.branch) ## Parent object couldn't use child's attributes
