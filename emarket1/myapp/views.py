from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request,'E-commerce-Home.html')

def blog(request):
    return render(request,'E-commerce-blog.html')
