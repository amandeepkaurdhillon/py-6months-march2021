# def cube(a):
#     return a**3

# print(cube(2))

# Lambda - It is an annonymous function used in advance python
x = lambda a:a**3
print(x(3))

## Write a lambda function that will return sum of two given numbers
y = lambda a,b:a+b
print(y(10,29))

z = lambda st:st.upper()
print(z("jhdgfegushjz"))
print(z("aMan"))