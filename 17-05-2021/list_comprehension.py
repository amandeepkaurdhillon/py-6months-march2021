# ls = []
# for i in range(1,101):
#     if i%2==0:
#         ls.append(i)

# a = [i for i in range(1,101)]
# a = ["hello" for i in range(1,101)]

a = [i for i in range(1,101) if i%2==0]
print(a)

# Add first 10 divisibles of 8 in list using list comprehension
a = [i for i in range(1,101) if i%8==0]
print(a)