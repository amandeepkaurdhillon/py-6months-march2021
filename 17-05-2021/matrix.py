a = [
    [10,20,30],
    [3,5,6],
    [33,45,3],
    [2,4,5]
]

# print(a[0][1])

for row in range(len(a)):
    for c in range(len(a[row])):
        print("a[{}][{}] = {}".format(row, c, a[row][c]))

# a[0][0]=10
# a[0][1]=20
# a[0][2]=30
# ...
# ...   
# a[1][2]=6
